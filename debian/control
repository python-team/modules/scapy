Source: scapy
Section: python
Priority: optional
Maintainer: David Villa Alises <David.Villa@uclm.es>
Uploaders: Francisco Moya <paco@debian.org>
Build-Depends: debhelper-compat (= 9),
               dh-python,
               python-all (>=2.6.6-3~),
               python-docutils,
               python-setuptools,
               python-sphinx (>= 1.0.7+dfsg-1~)
Standards-Version: 3.9.6
Homepage: http://www.secdev.org/projects/scapy/
Vcs-Git: https://salsa.debian.org/python-team/packages/scapy.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/scapy

Package: python-scapy
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}
Suggests: ebtables,
          graphviz,
          gv,
          hexer,
          imagemagick,
          librsvg2-bin,
          python-crypto,
          python-gnuplot,
          python-pcapy,
          python-pyx,
          python-visual,
          sox,
          tcpdump,
          tcpreplay,
          wireshark,
          xpdf
Replaces: scapy (<< 1.0.5-1)
Conflicts: scapy (<< 1.0.5-1)
Provides: scapy
Description: Packet generator/sniffer and network scanner/discovery
 Scapy is a powerful interactive packet manipulation tool, packet
 generator, network scanner, network discovery, packet sniffer, etc. It
 can for the moment replace hping, 85% of nmap, arpspoof, arp-sk, arping,
 tcpdump, tethereal, p0f, ....
 .
 In scapy you define a set of packets, then it sends them, receives
 answers, matches requests with answers and returns a list of packet couples
 (request, answer) and a list of unmatched packets. This has the big advantage
 over tools like nmap or hping that an answer is not reduced to
 (open/closed/filtered), but is the whole packet.

Package: python-scapy-doc
Architecture: all
Section: doc
Depends: ${misc:Depends}, ${sphinxdoc:Depends}
Description: Packet generator/sniffer and network scanner/discovery (documentation)
 Scapy is a powerful interactive packet manipulation tool, packet
 generator, network scanner, network discovery, packet sniffer, etc. It
 can for the moment replace hping, 85% of nmap, arpspoof, arp-sk, arping,
 tcpdump, tethereal, p0f, ....
 .
 In scapy you define a set of packets, then it sends them, receives
 answers, matches requests with answers and returns a list of packet couples
 (request, answer) and a list of unmatched packets. This has the big advantage
 over tools like nmap or hping that an answer is not reduced to
 (open/closed/filtered), but is the whole packet.
 .
 This is the documentation package.
